#!/bin/sh

APP_HOME=/home/ansible/mix2000beame/translate
APP_JAR=current
LOGS=$APP_HOME/logs

USR=$( whoami )

PID=$( ps -ea -o "pid ppid args" | grep -v grep | grep "java -jar $APP_JAR" | sed -e 's/^  *//' -e 's/ .*//' | head -1 )

_start() {
  if [ -z $PID ]; then
    echo "Starting Mix2000BeameTranslator..."
    cd $APP_HOME
    if [ "ansible" = "$USR" ]; then
      mkdir -p $LOGS ; nohup java -jar $APP_JAR 1>$LOGS/stdout.log 2>$LOGS/stderr.log &
    else
      sudo su -l devops -c "mkdir -p $LOGS ; nohup java -jar $APP_JAR 1>$LOGS/stdout.log 2>$LOGS/stderr.log &"
    fi
    cd -
    echo "Started Mix2000BeameTranslator."
  else
    echo "Mix2000BeameTranslator already running with PID: ${PID}"
  fi
}

_stop() {
  echo "Stopping Mix2000BeameTranslator..."
  if [ -z $PID ]; then
    echo "Mix2000BeameTranslator already stopped"
  else
    if [ "ansible" = "$USR" ]; then
      kill $PID
    else
      sudo kill $PID
    fi
    unset PID
  fi
}

_status() {
  test $PID && echo "Mix2000BeameTranslator is running with PID: ${PID}" || echo "Mix2000BeameTranslator is not running"
}

case "$1" in
  start)
    _start
    ;;
  stop)
    _stop
    ;;
  restart)
    _stop
    wait 3
    _start
    ;;
  status)
    _status
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
    ;;
esac
